#!/bin/bash
# Script to run full Athena builds inside docker containers
# Beware the code here highly relies on multiple variable defined inside the
# jenkins job configuration
exit_msg() {
    local message="$1"
    local code="${2:-1}"
    echo -e "${script_name}: ${message}"
    [[ -d $cwd ]] && cd $cwd
    exit "${code}"
}

function checkEOS() {
    ls /eos/atlas > /dev/null 2>&1 ; st_eosfuse=$?
    if [ "$st_eosfuse" -eq 0 ]; then
	echo "=== container EOS FUSE is OK"
    else
	echo "=== container EOS FUSE is broken (${st_eosfuse}). Try w/ leading eos"
	eos ls -l /eos/atlas
    fi
    return $st_eosfuse
}

script_name=`basename "${0%.*}"`
cwd=$(pwd)
if [ -x /usr/bin/id ] && [ x"$USER" = x ]; then
    export USER="`/usr/bin/id -un`"
fi


kinit atnight@CERN.CH -5 -V -k -t /build1/jenkins/atnight.keytab
klist
eosfusebind -g
echo "=== eosfuseboud -S"
eosfusebind -S
[[ -d /var/run/eosd/credentials/store ]] && ls -la /var/run/eosd/credentials/store

[[ -z "$mode" ]] && exit_msg "\"${mode}\ not set" ${ERR_EXIT:1}
[[ -z "$comp" ]] && exit_msg "\"${comp}\ not set" ${ERR_EXIT:1}

[[ -d $release_area ]] && rm -rf $release_area
mkdir -p $release_area; cd $release_area
[[ "${ATLAS_LOCAL_ROOT_BASE}" = "" ]] && export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup none,${comp},${mode} --cmakesetup
lsetup git

checkEOS ; st_check=$?
[[ "$st_check" = "0" ]] || exit_msg "exit after EOS check" ${st_check}

# Check if USER is in environments
env | egrep "^USER="

echo "=== STARTING git clone -b $branch https://:@gitlab.cern.ch:8443/atlas/athena.git" $(date) $release_area
 git clone -b $branch https://:@gitlab.cern.ch:8443/atlas/athena.git 2>&1 | tee checkout.log

echo "=== STARTING $project Externals Build" $(date) 
time ./athena/Projects/$project/build_externals.sh  $DEBUG

checkEOS ; st_check=$?
[[ "$st_check" = "0" ]] || exit_msg "exit after EOS check" ${st_check}

cmake --version
echo "=== Subs in athena/Database/ConnectionManagement/AtlasAuthentication/CMakeLists.txt ==="
echo "WORKSPACE: \"${WORKSPACE}\""
targ=athena/Database/ConnectionManagement/AtlasAuthentication/CMakeLists.txt
[[ -n $WORKSPACE ]] && [[ -f $targ ]] && [[ -d $WORKSPACE/AtlasAuth ]] \
 && sed -i "s,if( ENV{ATLAS_EXTERNAL} ),if( DEFINED ENV{ATLAS_EXTERNAL} )," $targ \
 && sed -i '/atlas_install_xmls( ${AtlasAuthentication_home}/i message("AtlasAuthentication_home " ${AtlasAuthentication_home})' $targ \
 && export ATLAS_EXTERNAL=$WORKSPACE \
 &&  grep "ENV{ATLAS_EXTERNAL" $targ
[[ -n $ATLAS_EXTERNAL ]] && echo "ATLAS_EXTERNAL: ${ATLAS_EXTERNAL}"


echo "=== STARTING $project Build" $(date)
./athena/Projects/$project/build.sh  $DEBUG

# Save BINARY_TAG for export
[[ -n $BINARY_TAG ]] && echo $BINARY_TAG >${WORKSPACE}/binary_tag_${BUILD_ID}

#take datestamp from ReleaseData file and set epoch too
if [ -f $release_area/build/install/$project/*/InstallArea/$BINARY_TAG/ReleaseData ]; then
    datestamp=`grep date $release_area/build/install/$project/*/InstallArea/$BINARY_TAG/ReleaseData |tr ":" " " |awk '{print $2}'`
    datestamp_minus_T=`echo $datestamp |tr "T" " "`
    epoch=`date --date="$datestamp_minus_T" +%s.%N`
else
    echo "Something went wrong and ReleaseData can not be read and datestamp set. Please check"
    exit 1
fi

echo "=== STARTING Release Unit Testing" $(date)
logUnitTesting=${WORKSPACE}/UnitTesting_${BUILD_ID}.log
echo "=== LOG FILE of Unit Testing: ${logUnitTesting}"
### DVA 2019-10-23 nstreans is left 16(in the regular 21.0 branch was 24)
[[ -x /build1/atnight/bin/unisystem_com_wrapper.sh ]] && /build1/atnight/bin/unisystem_com_wrapper.sh 180 /build/atnight/bin/unisystem_unit_tester_a.sh --relarea $release_area --project $project --nstreams 16 > ${logUnitTesting} 2>&1


echo "Container job is finished"
exit 

